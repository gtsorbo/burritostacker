//
//  BStackAppDelegate.h
//  Burrito Stacker
//
//  Created by block7 on 1/28/14.
//  Copyright (c) 2014 SICK Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BStackAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
