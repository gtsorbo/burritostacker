//
//  BStackHomeMenuViewController.m
//  Burrito Stacker
//
//  Created by block7 on 3/13/14.
//  Copyright (c) 2014 SICK Inc. All rights reserved.
//

#import "BStackHomeMenuViewController.h"

@interface BStackHomeMenuViewController ()

@end

@implementation BStackHomeMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
