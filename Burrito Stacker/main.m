//
//  main.m
//  Burrito Stacker
//
//  Created by block7 on 1/28/14.
//  Copyright (c) 2014 SICK Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BStackAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BStackAppDelegate class]));
    }
}
