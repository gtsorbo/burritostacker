//
//  BStackMyScene.m
//  Burrito Stacker
//
//  Created by block7 on 1/28/14.
//  Copyright (c) 2014 SICK Inc. All rights reserved.
//

#import "BStackMyScene.h"

@interface BStackMyScene () <SKPhysicsContactDelegate>

@property (nonatomic) NSString *selfie;

@property (nonatomic) SKSpriteNode * basket;
@property (nonatomic) SKLabelNode * calorieLabel;
@property (nonatomic) SKSpriteNode * rightClickArea;
@property (nonatomic) SKSpriteNode * leftClickArea;

@property (nonatomic) SKSpriteNode * bg1;
@property (nonatomic) SKSpriteNode * bg2;

@property (nonatomic) SKAction *moveBasketRight;
@property (nonatomic) SKAction *moveBasketLeft;
@property (nonatomic) SKAction *moveBasketRightForever;
@property (nonatomic) SKAction *moveBasketLeftForever;

@property (nonatomic) SKAction *leftSwayAction;
@property (nonatomic) SKAction *rightSwayAction;
@property (nonatomic) SKAction *swayBackAction;


@property (nonatomic) BOOL movingLeft;
@property (nonatomic) BOOL movingRight;
@property (nonatomic) BOOL moving;


@property (nonatomic) SKSpriteNode *subChild;

@property (nonatomic) NSTimeInterval lastSpawnTimeInterval;
@property (nonatomic) NSTimeInterval lastUpdateTimeInterval;

//@property (nonatomic) int catchHeight;
@property (nonatomic) int totalHeight;
@property (nonatomic) int itemCount;
@property (nonatomic) double rotationCoefficient;

@property (nonatomic) NSMutableArray *allItemsArray;

@property (nonatomic) int totalCalorieCount;

@property (nonatomic) int barbacoaCalorieCount;
@property (nonatomic) int blackbeansCalorieCount;
@property (nonatomic) int carnitasCalorieCount;
@property (nonatomic) int cheeseCalorieCount;
@property (nonatomic) int chickenCalorieCount;
@property (nonatomic) int fajitasCalorieCount;
@property (nonatomic) int guacamoleCalorieCount;
@property (nonatomic) int steakCalorieCount;


@end

static const uint32_t basketCategory = 0x1 << 0;
static const uint32_t itemFallingCategory = 0x1 << 1;
static const uint32_t itemCaughtCategory = 0x1 << 2;


@implementation BStackMyScene

-(id)initWithSize:(CGSize)size {    
    if (self = [super initWithSize:size]) {
        /* Setup your scene here */
        
        //Background Tiles
        //self.bg1 = [SKSpriteNode spriteNodeWithImageNamed:@"ChipotleNapkin"];
        self.bg1 = [SKSpriteNode spriteNodeWithColor:[UIColor brownColor] size:self.size];
        self.bg1.anchorPoint = CGPointZero;
        self.bg1.position = CGPointMake(0, 0);
        [self addChild:self.bg1];
        
        //self.bg2 = [SKSpriteNode spriteNodeWithImageNamed:@"ChipotleNapkin"];
        self.bg2 = [SKSpriteNode spriteNodeWithColor:[UIColor blackColor] size:self.size];
        self.bg2.anchorPoint = CGPointZero;
        self.bg2.position = CGPointMake(0, self.bg1.size.height-1);
        [self addChild:self.bg2];
        
        //Calories (Score)
        self.totalCalorieCount = 0;
        
        self.itemCount = 0;
        
        self.barbacoaCalorieCount = 165;
        self.blackbeansCalorieCount = 120;
        self.carnitasCalorieCount = 220;
        self.cheeseCalorieCount = 100;
        self.chickenCalorieCount = 180;
        self.fajitasCalorieCount = 20;
        self.guacamoleCalorieCount = 170;
        self.steakCalorieCount = 190;
        
        self.allItemsArray = [[NSMutableArray alloc] init];
        
        //Basket Moving Controls
        self.moveBasketLeft = [SKAction moveTo:CGPointMake(self.basket.position.x-.01, 10) duration:.1];
        self.moveBasketRight = [SKAction moveTo:CGPointMake(self.basket.position.x+.01, 10) duration:.1];
        self.moveBasketLeftForever = [SKAction repeatActionForever:self.moveBasketLeft];
        self.moveBasketRightForever = [SKAction repeatActionForever:self.moveBasketRight];

        self.selfie = @"HYFR";
        
        self.backgroundColor = [SKColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.0];
        
        self.basket = [SKSpriteNode spriteNodeWithImageNamed:@"Basket.png"];
        self.basket.position = CGPointMake(160, 10);
        [self addChild:self.basket];
        
        //Calorie (score) Label
        self.calorieLabel = [SKLabelNode labelNodeWithFontNamed:@"AvenirNextCondensed-Bold"];
        self.calorieLabel.name = @"TotalCalories";
        self.calorieLabel.fontSize = 18;
        self.calorieLabel.fontColor = [UIColor whiteColor];
        self.calorieLabel.text = [NSString stringWithFormat:@"Calories: %d", self.totalCalorieCount];
        self.calorieLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
        self.calorieLabel.position = CGPointMake(self.size.width/2, self.size.height-50);
        [self addChild:self.calorieLabel];
        
        //Create Basket Physics Body
        self.basket.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(50, 40)];
        self.basket.physicsBody.dynamic = YES;
        self.basket.physicsBody.categoryBitMask = basketCategory;
        self.basket.physicsBody.contactTestBitMask = itemFallingCategory;
        self.basket.physicsBody.collisionBitMask = 0;
        //self.basket.physicsBody.usesPreciseCollisionDetection = YES;
        
        //Invisible Right Control
        self.rightClickArea = [SKSpriteNode spriteNodeWithColor:[SKColor colorWithRed:0.0 green:0.8 blue:0.6 alpha:0.0] size:CGSizeMake(160, 240)];
        self.rightClickArea.position = CGPointMake(240, 100);
        [self addChild:self.rightClickArea];
        
        //Invisible Left Control
        self.leftClickArea = [SKSpriteNode spriteNodeWithColor:[SKColor colorWithRed:0.7 green:0.2 blue:0.6 alpha:0.0] size:CGSizeMake(160, 240)];
        self.leftClickArea.position = CGPointMake(80, 100);
        [self addChild:self.leftClickArea];
        
        //Physics Ish
        self.physicsWorld.gravity = CGVectorMake(0, 0);
        self.physicsWorld.contactDelegate = self;

        NSLog(@"%@", self.selfie);
        
        self.swayBackAction = [SKAction rotateToAngle:0 duration:.075];
        
        //self.catchHeight = 27;
        self.totalHeight = self.basket.size.height + 1;
        
        //Background Music!!!
        [self runAction:[SKAction repeatActionForever:[SKAction playSoundFileNamed:@"TheJazzPiano.mp3" waitForCompletion:YES]]];
        
    }
    return self;
}

-(void)addFallingItem {
    
    int itemImageNumber = (arc4random() % 8) + 1;
    NSString *fileName = [NSString stringWithFormat:@"item%d.png", itemImageNumber];
    SKSpriteNode *item = [SKSpriteNode spriteNodeWithImageNamed:fileName];
    
    if (itemImageNumber == 1) {
        item.name = @"barbacoa";
    } else if (itemImageNumber == 2) {
        item.name = @"blackbeans";
    } else if (itemImageNumber == 3) {
        item.name = @"carnitas";
    } else if (itemImageNumber == 4) {
        item.name = @"cheese";
    } else if (itemImageNumber == 5) {
        item.name = @"chicken";
    } else if (itemImageNumber == 6) {
        item.name = @"fajitas";
    } else if (itemImageNumber == 7) {
        item.name = @"guacamole";
    } else if (itemImageNumber == 8) {
        item.name = @"steak";
    }
    
    item.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(item.size.width/2, item.size.height)];
    //item.physicsBody.dynamic = YES; // 2
    item.physicsBody.categoryBitMask = itemFallingCategory; // 3
    item.physicsBody.contactTestBitMask = basketCategory; // 4
    item.physicsBody.collisionBitMask = 0; // 5
    item.physicsBody.friction = 0.5;
    //item.physicsBody.linearDamping = 0.1;
    
    
    int minX = item.size.width / 2;
    int maxX = self.frame.size.width - item.size.width / 2;
    int rangeX = maxX - minX;
    int actualX = (arc4random() % rangeX) + minX;
    
    item.position = CGPointMake(actualX, self.frame.size.height + item.size.height/2);
    [self addChild:item];
    
    //Determine speed of the items
    //int minDuration = 2.0;
    //int maxDuration = 2.1;
    //int rangeDuration = maxDuration - minDuration;
    int actualDuration = 5.0;
    
    //(arc4random() % rangeDuration) + minDuration;
    
    // Create the actions
    SKAction * actionMove = [SKAction moveTo:CGPointMake(actualX, -item.size.height/2) duration:actualDuration];
    SKAction * actionMoveDone = [SKAction removeFromParent];
       [item runAction:[SKAction sequence:@[actionMove, actionMoveDone]] withKey:@"itemFalling"];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    
    
    for (UITouch *touch in touches) {
        CGPoint touchPoint = [touch locationInNode:self];
        if ([self.leftClickArea containsPoint:touchPoint]){
            [self.allItemsArray enumerateObjectsUsingBlock:^(SKSpriteNode *node, NSUInteger idx, BOOL *stop) {
                [node removeActionForKey:@"SwayRight"];
                [node removeActionForKey:@"SwayBackRight"];
                [node runAction:self.leftSwayAction withKey:@"SwayLeft"];
            }];
            self.movingLeft = YES;
            self.moving = YES;
            
        }
        if ([self.rightClickArea containsPoint:touchPoint]){
            [self.allItemsArray enumerateObjectsUsingBlock:^(SKSpriteNode *node, NSUInteger idx, BOOL *stop) {
                [node removeActionForKey:@"SwayLeft"];
                [node removeActionForKey:@"SwayBackLeft"];
                [node runAction:self.rightSwayAction withKey:@"SwayRight"];
            }];
            self.movingRight = YES;
            self.moving = YES;
        }
    }
    
}


-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    
    
    for (UITouch *touch in touches) {
        CGPoint touchPoint = [touch locationInNode:self];
        if ([self.leftClickArea containsPoint:touchPoint]){
            [self.allItemsArray enumerateObjectsUsingBlock:^(id object, NSUInteger idx, BOOL *stop) {
                [object runAction:self.swayBackAction withKey:@"SwayBackLeft"];
            }];
            self.movingLeft = NO;
            self.moving = NO;
        }
        if ([self.rightClickArea containsPoint:touchPoint]){
            [self.allItemsArray enumerateObjectsUsingBlock:^(id object, NSUInteger idx, BOOL *stop) {
                [object runAction:self.swayBackAction withKey:@"SwayBackRight"];
            }];
            self.movingRight = NO;
            self.moving = NO;
        }
    }
    
}


- (void)updateWithTimeSinceLastUpdate:(CFTimeInterval)timeSinceLast {
    
   // int spawnInterval = (arc4random() % 20) + 1;
    self.lastSpawnTimeInterval += timeSinceLast;
    if (self.lastSpawnTimeInterval > 1) {
        self.lastSpawnTimeInterval = 0;
        [self addFallingItem];
    }
}


-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
    
    if (self.itemCount < 0) {
        self.rotationCoefficient = (.07/(pow(self.itemCount, 0.95)));
    } else self.rotationCoefficient = 0.01;
    //NSLog(@"%f", self.rotationCoefficient);
    //NSLog(@"%d", self.itemCount);
    self.rightSwayAction = [SKAction rotateByAngle:(self.rotationCoefficient) duration:(.1*pow(self.itemCount+1, .1))];
    self.leftSwayAction = [SKAction rotateByAngle:(-self.rotationCoefficient) duration:(.1*pow(self.itemCount+1, .1))];
    
    if (self.movingRight && self.basket.position.x < self.size.width-(self.basket.size.width/2)) {
        self.basket.position = CGPointMake(self.basket.position.x + 6, self.basket.position.y);
        //[self.basket.physicsBody applyForce:CGVectorMake(10, 0)];
        
    }
    if (self.movingLeft && self.basket.position.x > self.basket.size.width/2) {
        self.basket.position = CGPointMake(self.basket.position.x - 6, self.basket.position.y);
        //[self.basket.physicsBody applyForce:CGVectorMake(-10, 0)];
        
    }
    
    
    //Scrolling Background
    if (self.bg1.position.y < -self.bg1.size.height){
        self.bg1.position = CGPointMake(self.bg2.position.x, self.bg1.position.y + self.bg2.size.height);
    }
    
    if (self.bg2.position.y < -self.bg2.size.height) {
        self.bg2.position = CGPointMake(self.bg1.position.x, self.bg2.position.y + self.bg1.size.height);
    }

    
    CFTimeInterval timeSinceLast = currentTime - self.lastUpdateTimeInterval;
    self.lastUpdateTimeInterval = currentTime;
    if (timeSinceLast > 1) { // more than a second since last update
        timeSinceLast  = 1 / 60.0;
        self.lastUpdateTimeInterval = currentTime;
    }
    
    self.calorieLabel.text = [NSString stringWithFormat:@"Calories: %d", self.totalCalorieCount];

    [self updateWithTimeSinceLastUpdate:timeSinceLast];
}

-(void)basket:(SKSpriteNode *)basket didCollideWithItem:(SKSpriteNode *)item {
   
    [self alignObject:item withObject:basket];
    
    if ([item.name  isEqual: @"barbacoa"]) {
        self.totalCalorieCount = self.totalCalorieCount + self.barbacoaCalorieCount;
    } else if ([item.name  isEqual: @"blackbeans"]) {
        self.totalCalorieCount = self.totalCalorieCount + self.blackbeansCalorieCount;
    } else if ([item.name  isEqual: @"carnitas"]) {
        self.totalCalorieCount = self.totalCalorieCount + self.carnitasCalorieCount;
    } else if ([item.name  isEqual: @"cheese"]) {
        self.totalCalorieCount = self.totalCalorieCount + self.cheeseCalorieCount;
    } else if ([item.name  isEqual: @"chicken"]) {
        self.totalCalorieCount = self.totalCalorieCount + self.chickenCalorieCount;
    } else if ([item.name  isEqual: @"fajitas"]) {
        self.totalCalorieCount = self.totalCalorieCount + self.fajitasCalorieCount;
    } else if ([item.name  isEqual: @"guacamole"]) {
        self.totalCalorieCount = self.totalCalorieCount + self.guacamoleCalorieCount;
    } else if ([item.name  isEqual: @"steak"]) {
        self.totalCalorieCount = self.totalCalorieCount + self.steakCalorieCount;
    }
    
    [item removeFromParent];
    [basket addChild:item];
    [self.allItemsArray addObject:item];
    self.itemCount = self.itemCount + 1;
    item.physicsBody.categoryBitMask = basketCategory;
    item.physicsBody.contactTestBitMask = itemFallingCategory;
    self.totalHeight = self.totalHeight + (item.size.height*.45);
    
    self.basket.physicsBody.categoryBitMask = itemCaughtCategory;
    basket.physicsBody.categoryBitMask = itemCaughtCategory;
    //basket.physicsBody.contactTestBitMask = itemCaughtCategory;
    
    
    //NSLog(@"%d", self.totalHeight);
    //NSLog(@"%f", item.position.y);
    
    
    if (self.totalHeight >= self.size.height/2) { 
        [self.basket runAction:[SKAction moveToY:self.basket.position.y - item.size.height*.45 duration:.2]];
        [self.bg1 runAction:[SKAction moveToY:self.bg1.position.y-5 duration:.2]];
        [self.bg2 runAction:[SKAction moveToY:self.bg2.position.y-5 duration:.2]];
        
        }
}

-(void)alignObject:(SKSpriteNode *)objectA withObject:(SKSpriteNode *)objectB {
    
    [objectA runAction:[SKAction moveTo:CGPointMake(0, (objectA.size.height*.45)) duration:.2]];
    //NSLog(@"Align Did Run");
}

-(void)didBeginContact:(SKPhysicsContact *)contact
{
    // 1
    SKPhysicsBody *firstBody, *secondBody;
    
    if (contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask)
    {
        firstBody = contact.bodyA;
        secondBody = contact.bodyB;
    }
    else
    {
        firstBody = contact.bodyB;
        secondBody = contact.bodyA;
    }
    
    // If a falling item collides with the basket
    if ((firstBody.categoryBitMask & basketCategory) !=0 && (secondBody.categoryBitMask & itemFallingCategory) !=0)
    {
        [secondBody.node removeActionForKey:@"itemFalling"];
        [self basket:(SKSpriteNode *)firstBody.node didCollideWithItem:(SKSpriteNode *)secondBody.node];
    }
}

@end
